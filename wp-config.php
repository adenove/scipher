<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
/** name online databae == scsc1943_scipher local = scipher*/
define( 'DB_NAME', 'scsc1943_new' );

/** MySQL database username */
/** online databae usernmae == scsc1943_root local = root*/
define( 'DB_USER', 'scsc1943_root' );

/** MySQL database password */
/** online databae password == {gJ@szpcTWx! local = ""*/
define( 'DB_PASSWORD', '{gJ@szpcTWx!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'WGp@!-}PQrbIpDp=T{kiW,E ?MesVu*tyY=przn}~qVVQu:ud`Z}hi{SQ7FKoI|w' );
define( 'SECURE_AUTH_KEY',  '0FQp-xpwV&MXcS)g8Hdp5TLFWe,`Xg/!ks8d:q|Qe5pW&`?ID)!P+].@B_B% drK' );
define( 'LOGGED_IN_KEY',    '8}o!mY~*=jKNY|1sLm?s/fs^k[?0@NqrK^]w%UO#9_d%P>7&CH&;aNbKENTey?6;' );
define( 'NONCE_KEY',        'W1/+dF[X@)>!F{9c,Cb&GjTemdEB?PmqbT#]d=BE y[JQL5=%;B0ZZ^Cp#VA_1-F' );
define( 'AUTH_SALT',        ']y?=Ew,L_0<7|aUx-xL#`VUn<RKr%db1_ki(ArSQ:Vl|zAIFcWL{<-j:pOvz.U.*' );
define( 'SECURE_AUTH_SALT', 'gzG#WAq_Su:PJ_~.EE {*S6[OK`/![/2Ojg,,xJ-{55[!Jfkv8PzkK=bbln,YgI0' );
define( 'LOGGED_IN_SALT',   '3dDI|j`nM`_Pg~#p/ti*m_O$YL0dfJBZ1YhNLA0paZ(rw %E)Y6fAn;7dE(eVJc|' );
define( 'NONCE_SALT',       'ZujA )y)}vapSb!n0=44Kdvqs:w:<[N.1:|yFxnh//WkT]})A<vAb3nBtl`yPjl@' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
