<?php 

abstract class Youtube_Meta_Box {
   
   
    /**
     * Set up and add the meta box.
     */
    public static function add() {
        $screens = [ 'videos'];
        foreach ( $screens as $screen ) {
            add_meta_box(
                'youtube_link_id',          // Unique ID
                'Link Youtube', // Box title
                [ self::class, 'html' ],   // Content callback, must be of type callable
                $screen                  // Post type
            );
        }
    }
 
 
    /**
     * Save the meta box selections.
     *
     * @param int $post_id  The post ID.
     */
    public static function save( int $post_id ) {
        if ( array_key_exists( 'link_youtube', $_POST ) ) {
            update_post_meta(
                $post_id,
                'link_youtube_meta_key',
                $_POST['link_youtube']
            );
        }
    }
 
 
    /**
     * Display the meta box HTML to the user.
     *
     * @param \WP_Post $post   Post object.
     */
    public static function html( $post ) {
        $value = get_post_meta( $post->ID, 'link_youtube_meta_key', true );
        ?>
        
        <input 
        
            style="width:100%;padding:10px;" 
            type="text" 
            value="<?php echo $value;?>"
            name="link_youtube" 
            id="link_youtube" 
            placeholder="Input Link Youtube" >
        
        <?php
    }
}
 
add_action( 'add_meta_boxes', [ 'Youtube_Meta_Box', 'add' ] );
add_action( 'save_post', [ 'Youtube_Meta_Box', 'save' ] );

;?>