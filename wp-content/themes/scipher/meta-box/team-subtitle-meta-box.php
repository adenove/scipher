<?php 

abstract class TeamSubtitleMetaBox {
   
   
    /**
     * Set up and add the meta box.
     */
    public static function add() {
        $screens = [ 'team'];
        foreach ( $screens as $screen ) {
            add_meta_box(
                'team_subtitle_key',          // Unique ID
                'Info Team', // Box title
                [ self::class, 'html' ],   // Content callback, must be of type callable
                $screen                  // Post type
            );
        }
    }
 
 
    /**
     * Save the meta box selections.
     *
     * @param int $post_id  The post ID.
     */
    public static function save( int $post_id ) {
        if ( array_key_exists( 'subtitle', $_POST ) ) {
            update_post_meta(
                $post_id,
                'team_subtitle_key',
                array(
                    "position"=>$_POST['position'],
                    "subtitle"=>$_POST['subtitle'],
                )
            );
        }
    }
 
 
    /**
     * Display the meta box HTML to the user.
     *
     * @param \WP_Post $post   Post object.
     */
    public static function html( $post ) {
        $value = get_post_meta( $post->ID, 'team_subtitle_key', true ); 
        ?>    
        <label for="position"> Position </label>
        <input 
        style="width:100%;padding:10px;margin-bottom:20px;" 
        type="text" 
        name="position" 
        id="position" 
        value="<?php 
            if($value != ""){
                echo $value["position"];
            }        
        ?>"
        placeholder="Input Position" 
        >
        <label for="subtitle"> Subtitle </label>
        <textarea 
            style="width:100%;padding:10px;margin-bottom:20px;" 
            type="text" 
            name="subtitle" 
            id="subtitle" 
            placeholder="Input Subtitle" >
            <?php 
                if($value != ""){
                    echo $value["subtitle"];
                }
            ;?>
        </textarea>  

        <?php
    }
}
 
add_action( 'add_meta_boxes', [ 'TeamSubtitleMetaBox', 'add' ] );
add_action( 'save_post', [ 'TeamSubtitleMetaBox', 'save' ] );

;?>