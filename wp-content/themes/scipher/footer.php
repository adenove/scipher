<?php $footer = new FooterMenu();?>

  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <?php $footer->get();?>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="me-md-auto text-center text-md-start">
        <div class="copyright py-2">
          &copy; 2021  <strong><span>Scipher Technologies Pty Ltd</span></strong>, ABN 63 134 306 942 ("Scipher")
        </div>
      </div>
      <div class="d-md-flex text-center">
        <div class="px-2 py-2">
          <a href="<?php echo get_site_url();?>/privacy-policy/" class="text-white" >Privacy Policy</a>
        </div>
        <div class="px-2 py-2">
          <a href="<?php echo get_site_url();?>/terms-and-conditions-policy" class="text-white" >Terms & Conditions</a>
        </div>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>

</body>

</html>