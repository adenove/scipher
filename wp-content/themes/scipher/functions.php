<?php 
  
  include_once("config/page-config.php");
  include_once("config/post-type.php");
  include_once("config/template-config.php");
  include_once("config/posts.php");
  include_once("config/header-menu.php");
  include_once("config/footer-menu.php");
  include_once("meta-box/meta-box-youtube.php");
  include_once("meta-box/team-subtitle-meta-box.php");

  $postType = new PostType();

  //add featured image
  add_theme_support( 'post-thumbnails' );
  
  //benner image
  $thumb = new MultiPostThumbnails(
    array(
      'label' => 'Banner Image',
      'id' => 'banner-image',
      'post_type' => 'page'
    )
  );
  
  //ecxerpt
  function excerpt($num) {
    if(strlen(get_the_excerpt()) > $num){
      $limit = $num+1;
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      array_pop($excerpt);
      $excerpt = implode(" ",$excerpt)."...";
      echo $excerpt;
    }else {
      echo get_the_excerpt();
    }
  }

  // register custom post type 

  //service page
  foreach(PageConfig::$ids as $id){
    $postType->registerPostType($id["id_page"]);
  }

  //case-studies
  $postType->registerPostType(87);

  //videos
  $postType->registerPostType(89);

  //latest-news
  $postType->registerPostType(124);
  
  //thought-leader-ship
  $postType->registerPostType(126);
  
  //team
  $postType->registerPostType(104);


  //youtube
  function getYoutubeIdFromUrl($url) {
    $parts = parse_url($url);
    
    if(isset($parts['query'])){
        parse_str($parts['query'], $qs);
        if(isset($qs['v'])){
            return $qs['v'];
        }else if(isset($qs['vi'])){
            return $qs['vi'];
        }
    }
    
    if(isset($parts['path'])){
        $path = explode('/', trim($parts['path'], '/'));
        return $path[count($path)-1];
    }

    return false;
  }

  function get_youtube_thumbnail_url($url){
    $id = getYoutubeIdFromUrl($url);
    
    if($url != false){
      return "https://img.youtube.com/vi/".$id."/mqdefault.jpg";
    }

    return null;
  }

function custom_pagination($paged = 0,$max = 0) {  

  echo get_pagenum_link($paged - 1);
  for ($i=1; $i <= $max; $i++) { 
    echo $i;
  }
}

add_action('send_headers', function(){
  // Enforce the use of HTTPS
  header("Strict-Transport-Security: max-age=31536000; includeSubDomains");
  // Prevent Clickjacking
  header("X-Frame-Options: SAMEORIGIN");
  // Block Access If XSS Attack Is Suspected
  header("X-XSS-Protection: 1; mode=block");
  // Prevent MIME-Type Sniffing
  header("X-Content-Type-Options: nosniff");
  // Referrer Policy
  header("Referrer-Policy: no-referrer-when-downgrade");
}, 1);