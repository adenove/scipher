<?php
/*
  Template Name: Clients layout
  Template Post Type: clients-post
*/
;?>
<?php $template = new TemplateConfig();?>
<?php get_header();?>
  
  <main id="main">
    <?php $template->component("image-header-big.php");?>
    <section id="single" class="single section">
      <?php $template->component("breadchumbs.php");?>
      <div class="container">
        <div class="row">
          <div class="row align-items-center mb-3">
            <div class="col-md-7 col-xs-12 text-center">
              <p><?php the_content();?></p>
            </div>
            <div class="col-md-5 d-flex justify-content-end">
              <form class="search-form" action="<?php echo home_url();?>" role="search">
                <input type="text" name="s" id="search" placeholder="Search" value="<?php the_search_query(); ?>" />
                <input hidden type="text" name="post_type" id="search" value="<?php echo $post->post_name;?>" />  
                <input hidden type="text" name="ID" id="search" value="<?php echo $post->ID;?>" />  
                <button class="submit-search" type="submit" ><i class="bi bi-search"></i></button>
              </form>
            </div>
          </div>
          <hr>
          <div class="col-md-12">
            <section id="team" class="team py-4">
              <div class="container">
                <div class="row">
                  <?php 
                    $query = new WP_Query( 
                      array( 
                        'post_type' => $post->post_name,
                        "order" => "ASC" ,
                        'posts_per_page' => 9,
                        'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
                        's' => the_search_query()
                      )
                    );

                    while ( $query->have_posts() ) { $query->the_post();?>
                      
                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="member" data-aos="fade-up">
                        <?php $template->component("image-thumbnail.php");?>
                        <div class="member-info">
                          <h4><?php the_title();?></h4>
                          <span class="mb-3" ><?php the_time( 'D, j F y, g:i a');?></span>
                          <span><?php excerpt(18);?></span>
                          <div class="mt-3">
                            <a id="button-green" href="<?php the_permalink();?>">Read More</a>
                          </div>
                        </div>
                      </div>
                    </div>    
                  
                    <?php }; ?>

                  <?php if(!$query->have_posts()) {?> <!-- endwhile -->
                    <p class="text-center" >Post Not Found.</p>
                  <?php }?>

                  <?php if($query->max_num_pages > 1) : ?>
                  
                    <div class="pagination mt-5">
                    <?php 
                      $big = 999999999;
                      echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'prev_text'          => __('Previous'),
                        'next_text'          => __('Next '),
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $query->max_num_pages
                      ));
                      
                      ;?>
                    </div>

                  <?php endif;?>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </section>
  </main>

<?php get_footer();?>
