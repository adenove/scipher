<?php /* Template Name: Page Full */ ?>
<?php $template = new TemplateConfig();?>

<?php get_header();?>
  <main id="main">
    <section id="single" class="single section m-90">
      <?php $template->component("breadchumbs.php");?>
      <div class="container">
        <div class="row pb-5">
          <div class="col-md-12">
            <div class="mb-4">
            <?php $template->component("image-header-small.php");?>
            </div>
            <article> <?php the_content();?> </article>
          </div>
        </div>
      </div>
    </section>
  </main>
  <?php get_footer();?>