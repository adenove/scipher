<?php
  /*
    Template Name: Full-width layout
    Template Post Type: about-us
  */
;?>

<?php $template = new TemplateConfig();?>

<?php get_header();?>

  <main id="main">
    <?php $template->component("image-header-big.php");?>
    <section id="single" class="single section">
      <?php $template->component("breadchumbs.php");?>
      <div class="container">
        <div class="row">
          <div class="title w-100 text-center">
            <h2><?php the_title();?></h2>
            <div class="hr w-100"></div>
          </div>
          <div class="col-md-8 mt-5">
            <article> <?php the_content();?> </article>
          </div>
          <div class="col-md-4 mt-5">
           <?php $template->component("image-thumbnail.php");?>
          </div>
        </div>
      </div>
    </section>
  </main>

  <?php get_footer();?>
