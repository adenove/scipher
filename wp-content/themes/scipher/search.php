<?php
/*
  Template Name: News layout
  Template Post Type: news-post,clients-post
*/
;?>
<?php $template = new TemplateConfig();?>
<?php get_header();?>
  
  <main id="main">
  <div class="banner image-header">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/search.jpg" alt="image">
  </div>
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">
        <div class="d-flex justify-content-between align-items-center">
          <h2><a href="<?php echo get_permalink( $_GET["ID"]);?>"><?php echo $_GET["post_type"];?></a> / Search : <?php echo $_GET["s"];?></h2>
        </div>
      </div>
    </section>
    <section id="single" class="single section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-4">
            <form class="search-form" action="<?php echo home_url();?>" role="search">
              <input type="text" name="s" placeholder="search" id="search" value="<?php the_search_query(); ?>" />
              <input hidden type="text" name="post_type" id="search" value="<?php echo $_GET["post_type"];?>" />  
              <input hidden type="text" name="ID" id="search" value="<?php echo $_GET["ID"];?>" />  
              <button class="submit-search" type="submit" ><i class="bi bi-search"></i></button>
            </form>
          </div>
          <hr>
          <div class="col-md-12">
            <section id="team" class="team py-4">
              <div class="container">
                <div class="row">
                  <?php 
                    $query = new WP_Query( 
                      array( 
                        'post_type' => $_GET["post_type"],
                        "order" => "ASC" ,
                        'posts_per_page' => 9,
                        'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
                        's' => $_GET["s"],
                      )
                    );

                    while ( $query->have_posts() ) { $query->the_post();?>
                    
                    <?php if($_GET["post_type"] != "videos") { ?>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                      <div class="member" data-aos="fade-up">
                        <?php $template->component("image-thumbnail.php");?>
                        <div class="member-info">
                          <h4><?php the_title();?></h4>
                          <span class="mb-3" ><?php the_time( 'D, j F y, g:i a');?></span>
                          <span><?php excerpt(18);?></span>
                          <div class="mt-3">
                            <a id="button-green" href="<?php the_permalink();?>">Read More</a>
                          </div>
                        </div>
                      </div>
                    </div>    
                   
                    <?php } else { ?>
                    
                      <?php $link_youtube = get_post_meta($post->ID, "link_youtube_meta_key", true); ?>
                      
                      <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                        <div class="member" data-aos="fade-up">
                          <?php $template->component("image-youtube.php",null,get_youtube_thumbnail_url($link_youtube));?>
                          <div class="member-info">
                            <h4><?php the_title();?></h4>
                            <span class="mb-3" ><?php the_time( 'D, j F y, g:i a');?></span>
                            <span><?php excerpt(18);?></span>
                            <div class="mt-3">
                              <a id="button-green" href="<?php the_permalink();?>">Read More</a>
                            </div>
                          </div>
                        </div>
                      </div>  

                    <?php }; ?>

                  <?php } ;?>
                  
                  <?php if(!$query->have_posts()) {?> <!-- endwhile -->
                    <p class="text-center" >Post Not Found.</p>
                  <?php }?>

                  <?php if($query->max_num_pages > 1) : ?>
                  
                    <div class="pagination mt-5">
                    <?php 
                      $big = 999999999;
                      echo paginate_links( array(
                        'format' => '?paged=%#%',
                        'prev_text'          => __('Previous'),
                        'next_text'          => __('Next '),
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $query->max_num_pages
                      ));
                      
                      ;?>
                    </div>

                  <?php endif;?>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </section>
  </main>

<?php get_footer();?>
