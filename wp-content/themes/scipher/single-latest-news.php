<?php $template = new TemplateConfig();?>

<?php get_header();?>
  
  <main id="main">
    <?php $template->imageHeader(124);?>
    <section id="single" class="single section">
      <?php $template->breadchumbs("Latest News",124);?>
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="title">
              <h2><?php the_title();?></h2>
              <div class="hr"></div>
            </div>
            <article> 
              <span style="display:block;color:grey;margin-bottom:20px;" ><?php the_time( 'D, j F y, g:i a');?></span>
              <?php the_content();?> 
            </article>
          </div>
          <div class="col-md-4 pt-5">
            <?php $template->component("image-sidebar.php");?>
          </div>
        </div>
      </div>
    </section>
  </main>

<?php get_footer();?>