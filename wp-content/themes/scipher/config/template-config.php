<?php 

class TemplateConfig{
    
    public $page_name;

    
    function __construct($page_name = "") {
      $this->page_name = $page_name;
    }

    function component($component,$id_page = null,$youtube=null){
      $slug = get_post_type();
      $page = get_page_by_path($slug);


      if($page != null){
        $title = $page->post_title;
        $permalink = get_permalink($page->ID);
      }
      
      include(locate_template("/lib/component/" . $component));
    }

    function breadchumbs($title = "",$id = null){
      
      if($id != null){
        $permalink = get_permalink($id);
      }

      include(locate_template("/lib/component/breadchumbs.php"));
    }

    function imageHeader($id){
      $url_img = wp_get_attachment_url(get_post_thumbnail_id($id));
      include(locate_template("/lib/component/image-header-big.php"));
    }

    function postCard($row){
      include(locate_template("/lib/component/post-card-row.php"));
    }


    function child($file){
      include(locate_template("/lib/page/".$this->page_name."/child"."/".$file));
    }

    function main(){
      include(locate_template("/lib/page/".$this->page_name."/index.php"));
    }
    
}?>