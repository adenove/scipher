<?php 

class PostType{
  
  function registerPostType($id){
    $post = get_post($id);
    $title = $post->post_title;
    $slug = $post->post_name;

    $labels = array(
      'name'                  => _x( $title, 'Post Type General Name', 'text_domain' ),
      'singular_name'         => _x( $title, 'Post Type Singular Name', 'text_domain' ),
      'menu_name'             => __( $title, 'text_domain' ),
      'name_admin_bar'        => __( $title, 'text_domain' ),
      'archives'              => __( $title.' '.'Item Archives', 'text_domain' ),
      'attributes'            => __( $title.' '.'Item Attributes', 'text_domain' ),
      'parent_item_colon'     => __( $title.' '.'Parent Item:', 'text_domain' ),
      'all_items'             => __( $title.' '.'All Items', 'text_domain' ),
      'add_new_item'          => __( $title.' '.'Add New Item', 'text_domain' ),
      'add_new'               => __( 'Add New', 'text_domain' ),
      'new_item'              => __( 'New Item', 'text_domain' ),
      'edit_item'             => __( 'Edit Item', 'text_domain' ),
      'update_item'           => __( 'Update Item', 'text_domain' ),
      'view_item'             => __( 'View Item', 'text_domain' ),
      'view_items'            => __( 'View Items', 'text_domain' ),
      'search_items'          => __( 'Search Item', 'text_domain' ),
      'not_found'             => __( 'Not found', 'text_domain' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
      'featured_image'        => __( 'Featured Image', 'text_domain' ),
      'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
      'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
      'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
      'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
      'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
      'items_list'            => __( 'Items list', 'text_domain' ),
      'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
      'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
      'attributes'            => 'Page Attributes'
    );
    
    $rewrite = array(
      'slug'        => str_replace("-","",$slug),
      'with_front'  => true,
      'pages'       => true,
      'feeds'       => true,
    );

    $args = array(
      'label'                 => __( $title, 'text_domain' ),
      'description'           => __( $title.' Description', 'text_domain' ),
      'labels'                => $labels,
      'hierarchical'          => true,
      'supports'              => array( 'title', 'editor','thumbnail','page-attributes',"revisions","excerpt"),
      'taxonomies'            => array( 'category', 'post_tag' ),
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => true,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'rewrite'               => $rewrite,
      'capability_type'       => 'page',
    );

    register_post_type($slug, $args );
  }

}?>