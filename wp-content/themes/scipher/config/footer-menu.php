<?php 

class FooterMenu {

	function get(){
		foreach (PageConfig::$ids as $id) {
			$this->menu($id["id_page"],$id["id_sub_menu"]);
		}
	}

	function menu($id_page,$id_subs = array()){
		$get_page = Posts::getById($id_page);

		if($get_page != null){

			echo "<div class='col footer-links'>";
			echo "<h4>". $get_page["title"] ."</h4>";
			echo "<ul>";

			$sub_menu = Posts::getByPostType($get_page["type"]);

			if(count($sub_menu) > 0){
				for ($i=0; $i < count($sub_menu); $i++) { 
					$get_sub = $sub_menu[$i];
					echo "<li><i class='bx bx-chevron-right'></i> <a href=".$get_sub["link"].">".$get_sub["title"]."</a></li>";
				}
			}

			echo "</ul>";
			echo "</div>";
      
		}

  }

};?>