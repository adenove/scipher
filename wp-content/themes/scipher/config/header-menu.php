<?php 


class HeaderMenu{


  function get(){
    foreach (PageConfig::$ids as $menu) {
      $this->menu($menu["id_page"]);
    }
  }

  function menu($id_page){
    $get_page = Posts::getById($id_page);
    
    if($get_page != null){
      
      $sub_menu = Posts::getByPostType($get_page["type"]);

      echo "<li class='dropdown'><a href=".$get_page["link"]."><span>". $get_page["title"] ."</span> ";
      if(count($sub_menu) > 0 && $id_page != 28){
        echo "<i class='bi bi-chevron-down'></i>";
      }
      echo "</a>";
      
      // sub menu
      if(count($sub_menu) > 0 && $id_page != 28){
        
        echo "<ul>";
        
        foreach ($sub_menu as $menu) {

          if($menu != null){ 
            echo "<li><a href=".$menu["link"].">".$menu["title"]."</a></li>";
          }
					
        }
        
        echo "</ul>";
      }
      
      echo "</li>";
    }

  }


} ;?>