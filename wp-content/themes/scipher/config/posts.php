<?php 

class Posts{

	public static function getById($id){
		global $wpdb;
		$result = $wpdb->get_results("SELECT post_title,post_name FROM $wpdb->posts WHERE id = $id");
		
		if(count($result) > 0){
			return array(
				"title" => $result[0]->post_title,
				"link" => get_the_permalink($id),
				"type" => $result[0]->post_name,
			);
		}

		return null;
	}

	public static function getByPostType($pType){
		global $wpdb;
		$result = $wpdb->get_results(
			"SELECT ID,post_title,post_name FROM $wpdb->posts 
			WHERE post_type = '$pType'
			AND post_status = 'publish' 
			ORDER BY menu_order ASC"
		);

		$submenu = [];

		for ($i=0; $i < count($result); $i++) { 
			array_push($submenu,array(
				"title" => $result[$i]->post_title,
				"link" => get_the_permalink($result[$i]->ID),
			));
		}
		
		return $submenu;
	}

}

;?>