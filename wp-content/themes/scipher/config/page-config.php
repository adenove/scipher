<?php 

class PageConfig{
    
  public static $ids = array(
    //Services & Products
    array("id_page" => 12, "id_sub_menu" => array(67,72,71,75,74,76,80)),

    //Clients
    array("id_page" => 15, "id_sub_menu" => array(87,89,229)),
    
    //Locations
    array("id_page" => 17, "id_sub_menu" => array(98,239)),
    
    //About us
    array("id_page" => 19, "id_sub_menu" => array(100,103,104,244,107,109,111)),
    
    //ESG & Stakeholders 
    array("id_page" => 21, "id_sub_menu" => array(112,114,116,248,250)),
    
    //Investors
    array("id_page" => 24, "id_sub_menu" => array(120,121,122)),
    
    //News
    array("id_page" => 26, "id_sub_menu" => array(124,126,127,129)),
  );

}?>