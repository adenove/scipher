<?php $template = new TemplateConfig();?>

<?php get_header();?>
  <main id="main">
    <?php $template->component("image-header-big.php");?>
    <section id="single" class="single section">
      <?php $template->component("breadchumbs.php");?>
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="title">
              <h2><?php the_title();?></h2>
              <div class="hr"></div>
            </div>
            <article> 
              <?php the_content();?> 
            </article>
          </div>
          <div class="col-md-4 pt-5">
            <?php $template->component("image-sidebar.php");?>
          </div>
        </div>
      </div>
    </section>
  </main>
  <?php get_footer();?>