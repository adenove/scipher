<?php
  /*
    Template Name: Team layout
    Template Post Type: about-us
  */
;?>

<?php $template = new TemplateConfig();?>

<?php get_header();?>

<main id="main">
  <?php $template->component("image-header-big.php");?>
  <section id="single" class="single section">
    <?php $template->component("breadchumbs.php");?>
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="title">
            <h2>
              <?php the_title();?>
            </h2>
            <div class="hr"></div>
          </div>
          <article>
            <?php the_content();?>
          </article>
        </div>
        <div class="col-md-4 pt-5">
          <?php $template->component("image-sidebar.php");?>
        </div>
      </div>
    </div>
    <div class="team mt-5">
      <div class="container">
        <div class="row">
          <?php $query = new WP_Query( array( 'post_type' => "team","order" => "ASC"));
                while ( $query->have_posts() ) { $query->the_post();?>

          <?php $team_info = get_post_meta($post->ID, "team_subtitle_key", true); ?>

          <div class="col-md-4 d-flex align-items-stretch">
            <div class="member" data-aos="fade-up">
              <?php $template->component("image-thumbnail.php");?>
              <div class="member-info">
                <h4 style="padding: 10px;">
                  <?php the_title();?>
                </h4>
                <div style="padding:1px;" class="hr w-100"></div>
                <h5>
                  <?php echo $team_info["position"];?>
                </h5>
                <p>
                  <?php echo $team_info["subtitle"];?>
                </p>
                <div style="padding:1px;" class="hr w-100"></div>
                <p>
                  <?php the_content();?>
                </p>
              </div>
            </div>
          </div>
          <?php };?>
          <!-- endwhile -->
        </div>
      </div>
    </div>
  </section>
</main>

<style>
  section {
    padding: 0px;
  }

  .team {
    background-color: #eaeaea;
    padding: 60px 0 50px 0;
  }

  .no-img-fluid {
    height: 300px !important;
  }

  .member-info h4 {
    text-transform: uppercase;
  }

  .member-info h5 {
    color: #1bbd36;
    font-size: 16px;
    margin: 5px 0;
  }

  .team .member .member-info {
    height: auto !important;
  }

  .team .member .member-img .thumb {
    width: 100%;
    height: 300px !important;
    object-fit: contain;
  }
  
</style>

<?php get_footer();?>