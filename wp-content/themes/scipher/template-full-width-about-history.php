<?php
  /*
    Template Name: about History layout
    Template Post Type: about-us
  */
;?>

<?php $template = new TemplateConfig();?>

<?php get_header();?>


<main id="main">
  <div class="history-button">
    <a href="javascript:;" id="back" class="button-nav shadow">
      Back
    </a>
    <a href="javascript:;" id="next" class="button-nav shadow">
      Next
    </a>
  </div>
  <?php $template->component("image-header-big.php");?>
  <section id="single" class="single section">
    <?php $template->component("breadchumbs.php");?>
    <div class="container">
      <div class="row">
        <div class="title w-100 text-center">
          <h2>
            <?php the_title();?>
          </h2>
          <div class="hr w-100"></div>
        </div>
        <div class="col-md-8 mt-5">
          <div class="container">
            <div class="timeline">
              <?php the_content();?>
            </div>
            <!-- <div class="timeline">
              <h2 id="2006">2006</h2>
              <ul>
                <li>
                  <h4>PGM Refiners Site</h4>
                  &nbsp;
                  PGM Refiners developed e-waste assets and established one of the largest facilities of its kind in
                  Australia.
                  PGM was backed by a large infrastructure fund.
                </li>
              </ul>
              <h2 id="2015">2015</h2>
              <div></div>
              <ul>
                <li style="display:none"></li>
                <li class="shadow">
                  <h4>Toxfree Site</h4>
                  &nbsp;
                  Toxfree purchased from PGM e-waste tangible and intangible assets and further developed the business.
                </li>
              </ul>
              <h2 id="2016">2016</h2>
              <ul>
                <li class="shadow">
                  <span id="dot">Chris Sayers, MD of Naissance Capital in Prague (Czech Republic), and retained advisor
                    to Europe's 2nd largest e-waste compliance scheme, developed a business model focused on
                    introduction of e-waste processing infrastructure to Australia.</span>
                  <span id="dot">Chris was also an Acting CFO of a Swiss-based technology company.</span>
                </li>
              </ul>
              <h2 id="2018">2018</h2>
              <div></div>
              <ul>
                <li style="display:none"></li>
                <li class="shadow">
                  <h4>Cleanaway Site</h4>
                  &nbsp;
                  CWY purchased from Toxfree e-waste assets and further developed the business.
                </li>
              </ul>
              <h2 id="2019">2019</h2>
              <ul>
                <li class="shadow">
                  <span id="dot">February 2019 Scipher Technologies (Scipher) was established.</span>
                  <span id="dot">Scipher facilitated multiple EU visits with select Australian client and partners, who
                    assisted in further</span>
                  <span id="dot">developing a business plan</span>
                </li>
              </ul>
              <h2 id="2020">2020</h2>
              <ul>
                <li class="shadow">
                  <h4>Scipher Site</h4>
                  &nbsp;
                  Scipher purchased from Cleanaway tangible and intangible e-waste assets with a strategy to enhance
                  value. The acquired site remains one of the largest accredited e-waste facilities in Australia
                </li>
                <li class="shadow"><span id="dot">Regional site – Scipher secured a site and commenced feasibility and
                    market studies for business expansion</span></li>
              </ul>
              <h2 id="2021">2021</h2>
              <ul>
                <li style="display:none"></li>
                <li class="shadow">
                  <span id="dot">Commenced operations at Australia’s largest and most advanced e-waste facility
                    (Melbourne - Dandenong)</span>
                  <span id="dot">Signed site lease</span>
                  <span id="dot">Secured Vic EPA E-waste operating license</span>
                  <span id="dot">Achieved site accreditation (AS/NZS 5377 E-waste)</span>
                  <span id="dot">Site certified (ISO14001 EMS, 45001 OHS)</span>
                </li>
              </ul>
            </div> -->
          </div>
        </div>
        <div class="col-md-4 mt-5">
          <?php $template->component("image-thumbnail.php");?>
        </div>
      </div>
    </div>
  </section>
</main>

<style>
  .button-nav {
    background: #1bbd36;
    border-radius: 4px;
    transition: all 0.4s;
    color: white;
    padding: 8px 20px;
    margin-right: 20px;
    font-weight: bold;
  }

  .button-nav:hover {
    color: #111;
  }

  .history-button {
    display: flex;
    position: fixed;
    bottom: 15px;
    left: 20px;
    z-index: 996;
  }
</style>

<script>
  var dataScroll = document.querySelectorAll(".timeline h2");

  let indexScroll = -1;

  var nextBtn = document.getElementById("next");
  var backBtn = document.getElementById("back");

  nextBtn.addEventListener("click", function () {
    indexScroll += 1;
    if (indexScroll < dataScroll.length) {
      var toScroll = dataScroll[indexScroll];
      window.scrollTo({
        top: toScroll.offsetTop + 400,
        behavior: 'smooth',
      });
    } else {
      indexScroll = 0;
      var toScroll = dataScroll[indexScroll];
      window.scrollTo({
        top: toScroll.offsetTop + 200,
        behavior: 'smooth',
      });
    }

    return false;
  });

  back.addEventListener("click", function () {
    indexScroll -= 1;
    if (indexScroll > -1) {
      var toScroll = dataScroll[indexScroll];
      window.scrollTo({
        top: toScroll.offsetTop + 400,
        behavior: 'smooth',
      });
    } else {
      indexScroll = dataScroll.length - 1;
      var toScroll = dataScroll[indexScroll];
      window.scrollTo({
        top: toScroll.offsetTop + 400,
        behavior: 'smooth',
      });
    }

    return false;
  });
</script>

<?php get_footer();?>