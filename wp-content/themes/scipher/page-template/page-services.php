<?php /* Template Name: Page Services */ ?>

<?php $template = new TemplateConfig();?>

<?php get_header();?>

  <main id="main">
    <?php $template->component("image-header-big.php");?>
    <section id="team" class="team section-bg">
      <?php $template->component("breadchumbs.php");?>
      <div class="container">
        <div class="row justify-content-center">

          <?php 
          
          $slug = $post->post_name;
          $query = new WP_Query( array( 'post_type' => $slug,"order" => "ASC","orderby"=>"menu_order"));
          $row = array("news-post","investors-post","locations-post","about-us","services-products");
          
          while ( $query->have_posts() ) { $query->the_post();?>

            <?php if(in_array($slug,$row)){ ?>
            
            <?php $template->postCard(3);?>

            <?php } else {;?>
            
            <?php $template->component("post-card.php");?>

            <?php }; ?>
  
          <?php };?> <!-- endwhile -->

        </div>
      </div>
    </section>
  </main>

<?php get_footer();?>