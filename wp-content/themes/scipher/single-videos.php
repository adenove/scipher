<?php $template = new TemplateConfig();?>

<?php get_header();?>
  
  <main id="main">
    <?php $template->imageHeader(89);?>
    <section id="single" class="single section">
      <?php $template->breadchumbs("Videos",89);?>
      <?php $link_youtube = get_post_meta($post->ID, "link_youtube_meta_key", true); ?>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <iframe 
              width="100%" 
              height="600" 
              src="https://www.youtube.com/embed/<?php echo getYoutubeIdFromUrl($link_youtube);?>" 
              frameborder="0" 
              gesture="media" 
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen>
            </iframe>
            <div class="title w-100 mt-4">
              <h2><?php the_title();?></h2>
              <div class="hr w-100"></div>
              <p style="color:grey" ><?php echo get_the_date( get_option('date_format') ); ?>  <?php echo the_time( 'H:i:s' );?></p>
            </div>
            <article> <?php the_content();?> </article>
          </div>
        </div>
      </div>
    </section>
  </main>

<?php get_footer();?>