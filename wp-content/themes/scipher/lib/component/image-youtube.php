<div class="member-img">
	<?php if($youtube != null) { ?>
		<img src="<?php echo $youtube; ?>" class="img-fluid thumb" alt="">
	<?php } else if(has_post_thumbnail()) {?>
		<img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid thumb" alt="">
	<?php } else {?>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/no-image.png" class="no-img-fluid" alt="">
	<?php };?>
</div>