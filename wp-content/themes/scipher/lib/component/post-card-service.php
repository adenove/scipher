<?php $template = new TemplateConfig();?>

<div class="col-lg-3 col-md-6 d-flex align-items-stretch">
  <div class="member" data-aos="fade-up">
    
    <!-- thumbnail  -->
    <?php $template->component("image-thumbnail.php");?>
    
    <!-- content  -->
    <div class="member-info">
      <h4><?php the_title();?></h4>
      
      <div class="mt-3">
        <a id="button-green" href="<?php the_permalink();?>">Read More</a>
      </div>
    </div>
  
  </div>
</div>    