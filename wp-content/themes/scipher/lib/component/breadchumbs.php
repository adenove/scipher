<section id="breadcrumbs" class="breadcrumbs">
  <div class="container">
    <div class="d-flex justify-content-between align-items-center">
      <?php if(!isset($title) && !isset($permalink)){ ?>
        <h2><?php the_title();?></h2>
      <?php } else { ?>
        <h2><a href="<?php echo $permalink;?>"><?php echo $title;?></a> > <?php the_title();?></h2>
      <?php } ?>
    </div>
  </div>
</section>
