<?php if(has_post_thumbnail()) {?>
    <img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid">
<?php }; ?>