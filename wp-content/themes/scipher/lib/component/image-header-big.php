<?php 

  $slug = get_post_type();
  $page = get_page_by_path($slug);

  if($page != null && $slug != "news-post" ){
    $url_img = wp_get_attachment_url(get_post_thumbnail_id($page->ID));
    $title = $page->post_title;
  }else{
    $title = $slug;
  }

?>

<?php if(!isset($url_img)) {?>
  
  <div class="banner image-header">
    <?php if(has_post_thumbnail()) { ;?>
      <img src="<?php the_post_thumbnail_url(); ?>" alt="image">
    <?php }else { ?>
      <img class="no-img" src="<?php echo get_template_directory_uri(); ?>/assets/img/no-image.png" alt="image">
    <?php }; ?>
  </div>

<?php } else { ?>
  
  <div class="banner image-header">
    <?php if($url_img != "") { ;?>
      <img src="<?php echo $url_img; ?>" alt="image">
    <?php }else { ?>
      <img class="no-img" src="<?php echo get_template_directory_uri(); ?>/assets/img/no-image.png" alt="image">
    <?php }; ?>
  </div>
  
<?php }?>