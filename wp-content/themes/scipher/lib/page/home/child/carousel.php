<section id="hero">
  <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <!-- Slide 1 -->
      <div class="carousel-item active" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/home.jpg);">
        <div class="carousel-container"></div>
      </div>
    </div>
    <ol style="display:none;" class="carousel-indicators" id="hero-carousel-indicators"></ol>
  </div>
</section>