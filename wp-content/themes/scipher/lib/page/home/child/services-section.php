<?php $template = new TemplateConfig("home");?>

<section id="team" class="team section-bg pt-5">
  <div class="container" data-aos="fade-up">
    <div class="row justify-content-center">

      <?php foreach(PageConfig::$ids as $id){

        $query = new WP_Query( array( 'page_id' => $id["id_page"] ) );
        
        while ( $query->have_posts() ) { $query->the_post();?>

          <?php $template->component("post-card-service.php");?>
        
        <?php };?> <!-- endwhile -->
        
        <?php wp_reset_postdata();?>
      <?php };?> <!-- endfor -->
    </div>
  </div>
</section>