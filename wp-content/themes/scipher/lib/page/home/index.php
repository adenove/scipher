<?php $template = new TemplateConfig("home");?>

<?php get_header();?>

<?php $template->child("carousel.php");?>

<div class="main">
    <?php $template->child("services-section.php");?>
</div>

<?php get_footer();?>
